# scrna_pipeline_step1_nfcore

## What it is

This is a clone and modification of the [nf-core scRNA](https://nf-co.re/scrnaseq) pipeline. It performs alignment of scRNA data using Cellranger (supported) as well as other aligners (currently not supported).

## Differences from nf-core scRNA pipeline

There are the following differences from the original pipeline:
- possibility to provide a folder and automatic creation of a sample list file
- support of "short" output, which outputs only important files to one folder
- support of fastqc for multiple replicates / index files
- support of cellranger index inside of genome

## Standalone usage

For standalone usage, install `nextflow` and run the pipeline:

   ```bash
   nextflow run https://gitlab.com/semares_pipelines/scrna_pipeline_step1_nfcore.git -profile docker --input_folder <input_folder> --aligner cellranger --genome GRCh38 --igenomes_base <genome_folder> --cellranger_index_genome 1 --outdir <output_folder>
   ```

The following parameters should be provided:
- `--input_folder` replaces `--input` parameter of the original pipeline. Contains a folder with .fastq files. The sample file is generated automatically
- `--aligner` provides aligner. The pipeline is tested with "cellranger" value only
- `--genome` contains a genome. See `conf/igenomes.config` for reference
- `--igenomes_base` contains path or URL of the genome folder. For a structure, refer to `conf/igenomes.config`
- `--cellranger_index_genome` tells that the cellranger index is contained in the genome folder (starindex). Provide a value 1 for it
- `--outdir` is an output folder

## Semares usage

To use the pipeline with Semares, You need the following steps:

- go to `persistance/workflow_adapter`
- clone the pipeline:
```
git clone https://gitlab.com/semares_pipelines/scrna_pipeline_step1_nfcore.git
```
- add a record to the `pipelines.yaml`:
```
- pipelineName: scrna_counts_2
  pipelineDescription: nfCore/scRNA pipeline modified for Semares
  pipelinePath: <semares_base>/persistence/workflow_adapter/scrna_pipeline_step1_nfcore/main.nf
  pipelineCommand: nextflow run -profile docker --aligner cellranger --cellranger_index_genome 1 --igenomes_base /projectbig/semares/reference_genomes --aligner cellranger --short_output 1 -resume
  pipelineParams:
  - paramName: Input dataset
    paramKey: --input_folder
    paramDescription: input dataset
    paramType: InputPath
    isMultiValue: false
    isRequired: true
  - paramName: Output Folder Path
    paramDescription: output path
    paramKey: --outdir
    paramType: OutputPath
    isMultiValue: false
    isRequired: true
  - paramName: genome
    paramDescription: genome to be used
    paramKey: --genome
    paramType: Text
    isMultiValue: false
    isRequired: true

```