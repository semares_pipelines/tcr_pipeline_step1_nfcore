//
// Check citeseq library and creates a channel file 
//

workflow CITESEQ_LIBRARY_CHECK {
    take:
    citeseq_library // file: /path/to/citeseq_library.csv
    reads // channel: [ val(meta), [ reads ] ]
    input_folder

    main:
    print("Starting citeseq library check")

    sample_list = reads.collect { it[0] }

    citeseq_library_updated_ch = Channel
       .fromPath(citeseq_library)
       .splitCsv(header:true)
       .map{ validate_library_row (it, sample_list, input_folder)}
       .collectFile (name: "citeseq_library.csv", keepHeader: true)
       .collect()
       .map{file(it[0])}

    emit:
    ch_citeseq_library = citeseq_library_updated_ch.first()                                    // file: [ path(fastq), val(meta), val(library_name) ]
}


// Function to get list of [ meta, [ fastq_1, fastq_2 ] ]
def validate_library_row(LinkedHashMap row, sample_list, input_folder) {
    // create meta map
    if (!sample_list.contains(row.sample)){
       exit 1, "ERROR: The sample ${row.sample} does not exist; available samples are ${sample_list}\n" 
    }
    return "fastqs,sample,library_type\n./,${row.sample},${row.library_type}\n"
}
