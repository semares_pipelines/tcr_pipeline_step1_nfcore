/*
 * Alignment with Cellranger
 */

include {CELLRANGER_MKGTF} from "../../modules/nf-core/cellranger/mkgtf/main.nf"
include {CELLRANGER_MKREF} from "../../modules/nf-core/cellranger/mkref/main.nf"
include {CELLRANGER_COUNT} from "../../modules/nf-core/cellranger/count/main.nf"
include {CELLRANGER_COUNT_CITESEQ} from "../../modules/nf-core/cellranger/count/main.nf"
include {MTX_TO_H5AD     } from "../../modules/local/mtx_to_h5ad.nf"

// Define workflow to subset and index a genome region fasta file
workflow CELLRANGER_ALIGN {
    take:
        mode
        fasta
        gtf
        cellranger_index
        ch_fastq
        ch_citeseq_features
        ch_citeseq_library        
    main:
        ch_versions = Channel.empty()
        ch_outs = Channel.empty()

        assert cellranger_index || (fasta && gtf):
            "Must provide either a cellranger index or both a fasta file ('--fasta') and a gtf file ('--gtf')."

        if (!cellranger_index) {
            // Filter GTF based on gene biotypes passed in params.modules
            CELLRANGER_MKGTF( gtf )
            ch_versions = ch_versions.mix(CELLRANGER_MKGTF.out.versions)

            // Make reference genome
            CELLRANGER_MKREF( fasta, CELLRANGER_MKGTF.out.gtf, "cellranger_reference" )
            ch_versions = ch_versions.mix(CELLRANGER_MKREF.out.versions)
            cellranger_index = CELLRANGER_MKREF.out.reference
        }

        // Obtain read counts
        if (mode == "rna"){
            CELLRANGER_COUNT (
                // TODO what is `gem` and why is it needed?
                ch_fastq.map{ meta, reads -> [meta + ["gem": meta.id, "samples": [meta.id]], reads] },
                cellranger_index
            )
            ch_versions = ch_versions.mix(CELLRANGER_COUNT.out.versions)
            ch_outs = ch_outs.mix(CELLRANGER_COUNT.out.outs)
        }
        else if (mode == "citeseq"){
            ch_fastq_ids = ch_fastq
                 .reduce{a, b -> a[0].id + "_" +  b[0].id}
            ch_fastq_singleend = ch_fastq
                 .reduce{a, b -> a[0].single_end || b[0].single_end}
            ch_fastq_grouped = ch_fastq
                 .map {meta, reads -> [["id": ch_fastq_ids.val, "single_end": ch_fastq_singleend.val], reads]}
                 .groupTuple(by: [0])
                 .map { meta, reads -> [ meta, reads.flatten() ] }
                 .view()
            ch_citeseq_library.view()
               
            CELLRANGER_COUNT_CITESEQ (
                // TODO what is `gem` and why is it needed?
                ch_fastq_grouped.map{ meta, reads -> [meta + ["gem": meta.id, "samples": [meta.id]], reads] },
                cellranger_index,
                ch_citeseq_features,
                ch_citeseq_library
            )
            ch_versions = ch_versions.mix(CELLRANGER_COUNT_CITESEQ.out.versions)
            ch_outs = ch_outs.mix(CELLRANGER_COUNT_CITESEQ.out.outs)
        }

    emit:
        ch_versions
        cellranger_out  = ch_outs
}
