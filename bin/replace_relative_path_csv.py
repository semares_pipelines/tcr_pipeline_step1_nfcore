#!/usr/bin/env python

"""Provide a command line tool to replace relative pathes to absolute pathes."""

import argparse
import csv
import logging
import sys
import os
from pathlib import Path

logger = logging.getLogger()

def replace_path_str(str):
   absolute_path = os.getcwd()
   return str.replace("./", absolute_path)

def is_relative_path(str):
   return str.startswith("./")

def replace_path(str):
    if is_relative_path(str):
       return replace_path_str(str)
    else:
       return str

def replace_relative_pathes(file_in):
    """
    Replaces relative pathes (elements starting from "./") to the absolute pathes (result of os. getcwd()). In-place.

    Arg:
        file_in (pathlib.Path): The given tabular samplesheet. The format can be CSV or TSV

    """

    csv_content = []
    with open(file_in, "r") as fin:

        ## header
        header = [x.strip('"') for x in fin.readline().strip().split(",")]

        ## Check sample entries
        for line in fin:
            lspl = [replace_path(x.strip().strip('"')) for x in line.strip().split(",")]
            csv_content.append(lspl)
            
    with open(file_in, "w") as fin:
       ## write header
       fin.write(",".join(header)+"\n")
       
       ## write lines
       for line in csv_content:
           fin.write(",".join(line)+"\n")
            

def parse_args(argv=None):
    """Define and immediately parse command line arguments."""
    parser = argparse.ArgumentParser(
        description="Replace relative pathes to absolute pathes in a tabular samplesheet (in-place)",
        epilog="Example: python replace_relative_path_csv.py samplesheet.csv",
    )
    parser.add_argument(
        "file_in",
        metavar="FILE_IN",
        type=Path,
        help="Tabular input samplesheet in CSV or TSV format.",
    )

    parser.add_argument(
        "-l",
        "--log-level",
        help="The desired log level (default WARNING).",
        choices=("CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG"),
        default="WARNING",
    )
    return parser.parse_args(argv)

def main(argv=None):
    """Coordinate argument parsing and program execution."""
    args = parse_args(argv)
    logging.basicConfig(level=args.log_level, format="[%(levelname)s] %(message)s")
    if not args.file_in.is_file():
        logger.error(f"The given input file {args.file_in} was not found!")
        sys.exit(2)
    replace_relative_pathes(args.file_in)


if __name__ == "__main__":
    sys.exit(main())
